from flask import Flask, render_template
from flask_bootstrap import Bootstrap
from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField
from wtforms.validators import DataRequired

app =  Flask(__name__)
app.config['SECRET_KEY'] = 'ESTA ES MI LLAVE'
bootstrap = Bootstrap(app)

#creando clase de formulario
class User(FlaskForm):
    name_user = StringField("Nombre de usuario", validators=[DataRequired()])
    submit = SubmitField("submit")

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/user', methods=['GET', 'POST'])
def f_user():
    user = None
    form = User()
    if form.validate_on_submit():
        user = form.name_user.data
        form.name_user.data = ''
    return render_template('user.html', user = user, form = form)


if __name__ == '__main__':
    app.run(debug=True)
